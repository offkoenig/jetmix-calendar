class Utils {
  /**
   * Отвечает на запрос
   * @param {any} res 
   * @param {boolean} success 
   * @param {any} data 
   */
  static async response(res, success, data) {
    return res.end(JSON.stringify({
      success: success,
      data: data
    }));
  }
}

module.exports = Utils;