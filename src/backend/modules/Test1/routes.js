const Utils = require("../Utils/Utils");

const obj = {
  foo: 123,
  bar: null,
  jopa: 'jopa'
};

app.get('/api/test1', async (req, res) => {
  return Utils.response(res, true, obj);
});