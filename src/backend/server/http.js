const { server: { port } } = require('./../../../config');

const http = require('http');
const Express = require('express');

const socketIo = require('socket.io');
const { WebApiServer } = require('osmium-webapi');

global.app = new Express();

app.set('trust proxy', 1);
app.set('view engine', 'pug'); 
app.set('views', './src/frontend/views');
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('body-parser').json());

app.use('/public', Express.static(`./dist`));

app.use(async (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', '*');
  res.setHeader('Access-Control-Allow-Methods', '*');

  next();
});

// Подключаем модули
require('./../modules');

app.get('*', async (req, res) => {
  return res.render('test');
})


const webServer = new http.Server(app);

const ioServer = socketIo(webServer);

// @ts-ignore
// TODO: Переделать на адекватный веб-сокет
const server = new WebApiServer(ioServer); 

require('./../events')(server);
require('./../middleware')(server);

webServer.listen(port);

console.log(`Запущен HTTP веб-сервер на порту '${port}'`);