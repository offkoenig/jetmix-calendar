const { server: { port } } = require('../../../config');
const Express = require('express');

global.app = new Express();

app.set('trust proxy', 1);
app.set('view engine', 'pug'); 
app.set('views', './src/frontend/views');
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('body-parser').json());

app.use('/public', Express.static(`./dist`));

app.use(async (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'devil.hougan.space');
  res.setHeader('Access-Control-Allow-Headers', '*');
  res.setHeader('Access-Control-Allow-Methods', '*');

  next();
});

// Подключаем модули
require('../modules');

app.get('*', async (req, res) => {
  return res.render('build');
})

app.listen(port);

console.log(`Запущен HTTPS веб-сервер на порту '${port}'`);