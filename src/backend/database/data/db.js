const mongoose = require('mongoose');
const mongoOpts = { useNewUrlParser: true, useUnifiedTopology: true };
const { dbName, dbHost, dbLogin, dbPassword, dbPort } = require('../config');

// const uri = `mongodb://${dbLogin}:${dbPassword}@${dbHost}:${dbPort}/${dbName}`;
// const uri = `mongodb://localhost:27017/rubike`;

const startDb = async () => {
  // try {
  //   await mongoose.connect(uri, mongoOpts);
  // } catch (err) {
  //   console.log('error: ' + err)
  // }
}

module.exports = {
  startDb
}