const MongoDB = require('./MongoDB');

module.exports = async (callback) => { 
    MongoDB
        .connect()
        .then(() => {
            console.log('Подключение к базе данных успешно');
            console.log('');
            callback();
        })
        .catch(error => console.log(error));
}