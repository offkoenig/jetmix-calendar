const { base } = require('./../../../config');

const moment = require('moment');
const { Db, MongoClient } = require('mongodb');

/** Унифицированный идентификатор подключения. */
const uri = `mongodb://${base.login}:${base.pass}@${base.host}:${27017}/${base.name}`; 
// const uri = `mongodb://localhost:27017/rubike`;

/** Параметры подключения. */
const options = {
    minSize: 20,
    poolSize: 100, 
    // reconnectTries: 10000,
    useNewUrlParser: true,
    socketTimeoutMS: 30000,
    // reconnectInterval: 2000, 
    connectTimeoutMS: 30000,
    useUnifiedTopology: true,
};

/**
 * Экземпляр соеденения с MongoDB.
 * @type {Db}
 */
let db;

/** Класс предназначен для управления MongoDB. */
class MongoDB {
    /** Выполнить подключение к базе данных. */
    static async connect() {
        const client = new MongoClient(uri, options);

        // client.on('connectionPoolCreated', console.dir);
        // client.on('connectionPoolClosed', console.dir);
        // client.on('connectionCreated', console.dir);
        // client.on('connectionReady', console.dir);
        // client.on('connectionClosed', console.dir);
        // client.on('connectionCheckOutStarted', console.dir);
        // client.on('connectionCheckOutFailed', console.dir);
        // client.on('connectionCheckedOut', console.dir);
        // client.on('connectionCheckedIn', console.dir);
        // client.on('connectionPoolCleared', console.dir);

        // Выполняем подключение клиента.
        await client.connect();

        // Устанавливаем соеденение с базой данных.
        db = client.db(base.base);

        // db.on('close', console.dir);
        // db.on('error', console.dir);
        // db.on('fullsetup', console.dir);
        // db.on('parseError', console.dir);
        // db.on('reconnect', console.dir);
        // db.on('timeout', console.dir);

        return db;
    }

    /**
     * @param {string} collection Название коллекции.
     */
    constructor(collection) {
        /** Название коллекции. */
        this.collection = collection;

        Object.freeze(this);
    }

    /**
     * Выполнить агрегацию к коллекции.
     * @param {any[]} pipeline Данные запроса.
     * @param {*} options Параметры запроса.
     */
    async aggregate(pipeline, options = {}) {
        const { collection } = this;

        /** Ответ базы данных. */
        const response = await db
            .collection(collection)
            .aggregate(pipeline, options)
            .toArray();

        // Если пустой ответ прерываем выполнение.
        if(!response.length)
            return response;

        return response.some(value => !!value)
            ? db
                .collection(collection)
                .aggregate(pipeline, options)
                .toArray()
            : response;
    }

    /** Выполнить подсчет документов в коллекции. */
    countDocuments(value) {
        return db
            .collection(this.collection)
            .countDocuments(value, {});
    }

    /** Создать индексы по спецификациям. */
    createIndexes(value) {
        return db
            .collection(this.collection)
            .createIndexes(value);
    }

    /**
     * Удалить один документ из коллекции.
     */
    deleteOne(value) {
        return db
            .collection(this.collection)
            .deleteOne(value);
    }

    /** Удалить из коллекции все документы, соответствующие фильтру. */
    deleteMany(value) {
        return db
            .collection(this.collection)
            .deleteMany(value);
    }

    /** Сбросить все индексы коллекции. */
    dropIndexes() {
        return db
            .collection(this.collection)
            .dropIndexes();
    }

    async findAll(filter) {
        const { collection } = this;

        const response = await db
            .collection(collection)
            .find(filter)
            .toArray();

        return response;
    }

    /** Найти один документ, удовлетворяющий заданным критериям запроса в коллекции или представлении. */
    async findOne(filter, options = {}) {
        const { collection } = this;

        /** Ответ базы данных. */
        const response = await db
            .collection(collection)
            .findOne(filter, options);

        return response;
    }

    /** Вставить документ в коллекцию. */
    async insertOne(value) {
        const { collection } = this;

        /** Временная метка. */
        const timestamp = +moment();

        return db
            .collection(collection)
            .insertOne(
                {
                    _created: timestamp,
                    _updated: timestamp,
                    ...value,
                },
            );
    }

    /** Обновить один документ в коллекции на основе фильтра. */
    async updateOne(filter, query) {
        /** Временная метка. */
        const timestamp = +moment();

        // Если данные запроса это массив из запроса агрегации.
        if(Array.isArray(query)) {
            // Добавляем к данным запроса обновление временной метки обновления документа.
            query.push({ $set: { _updated: timestamp } });
        } else if(query instanceof Object) {
            // Дополняем операцию установки данных временной меткой обновления документа.
            // Если нет операции установки данных она будет создана автоматически.
            query.$set = {
                ...query.$set,
                _updated: timestamp,
            };
        }

        return db
            .collection(this.collection)
            .updateOne(filter, query);
    }

    /** Обновить один документ и веруть его. */
    async findOneAndUpdate(filter, query, options) {
        /** Временная метка. */
        const timestamp = +moment();

        // Если данные запроса это массив из запроса агрегации.
        if(Array.isArray(query)) {
            // Добавляем к данным запроса обновление временной метки обновления документа.
            query.push({ $set: { _updated: timestamp } });
        } else if(query instanceof Object) {
            // Дополняем операцию установки данных временной меткой обновления документа.
            // Если нет операции установки данных она будет создана автоматически.
            query.$set = {
                ...query.$set,
                _updated: timestamp,
            };
        }

        return db
            .collection(this.collection)
            .findOneAndUpdate(filter, query, options);
    }
}

Object.freeze(MongoDB);

module.exports = MongoDB;
