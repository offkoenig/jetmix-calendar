import Vue from 'vue';
import VueRouter from 'vue-router';

import Main from './main.vue';
import Vuex from './vuex';
import Router from './router';

import IO from 'socket.io-client';
import WebApi from 'osmium-webapi';
import Axios from 'axios';

import moment from 'moment';
import 'moment/locale/ru';

window.axios = Axios;
window.api = require('./api'); 
window.moment = moment;

window.webApi = new WebApi.WebApiClient(IO('/'));

Vue.use(VueRouter);

new Vue({
  store: Vuex,
  router: Router,
  render: (h) => h(Main)
}).$mount('#app');