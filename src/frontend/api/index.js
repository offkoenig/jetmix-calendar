const { restApi: { host }} = require("../../../config");

module.exports = async (type, module, obj) => {
  const query = obj 
    ? '?' + Object.entries(obj)
      .map(([key, value]) => `${key}=${value}`)
      .join('&')
    : '';

  const shouldBody = type == 'post';

  const result = await axios[type](`${host}/api/${module}${shouldBody ? '' : query}`, obj);

  return result.data;
};