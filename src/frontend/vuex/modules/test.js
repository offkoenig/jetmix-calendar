const state = {
	daysInfo: [],
};

const mutations = {
	daysInfo: (state, payload) => {
		state.daysInfo = payload;
	},
};
const getters = {
	daysInfo: state => {
		return state.daysInfo;
	},
};


export default {
	namespaced: true,
	state: state,
	mutations: mutations,
	getters: getters
}