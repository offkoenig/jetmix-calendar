const fs = require('fs');

(async () => {
  /**
   * Если не существует стандартного конфига - создаём его
   */
  const configExists = await fs.existsSync('./config.js');
  if (!configExists) {
    await fs.copyFile('./config.js.example', './config.js', () => {
      // TODO: Событие созданного конфига
    });
  }

  require('./src/backend/server');
})(); 