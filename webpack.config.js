const path = require('path');

const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
  mode: 'development',
  entry: './src/frontend/main.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: 'http://localhost:40050/public/',
    filename: 'bundle.js'
  },
  stats: {
      hash: false,
      version: false,
      timings: true,
      children: false,
      errorDetails: false,
      entrypoints: false,
      performance: false,
      chunks: false,
      modules: false,
      reasons: false,
      source: false,
      publicPath: false,
      builtAt: false,
  },
  devServer: {
    port: 40050,
    contentBase: path.join(__dirname, '/dist'),
    hot: true,
    host:'0.0.0.0',
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    }
  },
  resolve: {
    extensions: ['.js', '.vue', '.json', '.scss'],
    alias: {
      styles: path.resolve('src/frontend/scss')
    }
  },
  module: {
    rules: [{
      test: /\.vue$/,
      use: [
        {
          loader: 'vue-loader'
        }
      ]
    }, {
        test: /\.pug$/,
        use: [ {loader: 'pug-plain-loader' } ],
    },
    {
        test: /\.css$/,
        use: [
            'vue-style-loader',
            'style-loader',
            'css-loader'
        ],
    }, 
    {
        test: /\.less$/,
        use: [
            'vue-style-loader',
            'css-loader',
            'less-loader',
        ],
    },
    {
        test: /\.scss$/,
        use: [
            'vue-style-loader',
            'css-loader',
            'sass-loader',
        ],
    },
    {
        test: /\.(gif|png|jpe?g|webp)$/,
        use: [
          {
              loader: 'file-loader',
              options: {
                  outputPath: 'images',
                  name: '[contenthash].[ext]',
                  esModule: false
              },
          },
        ],
    },
    {
      test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }
      ]
    }]
  },
  plugins: [
    new VueLoaderPlugin()
  ]
};